/*
 * Copyright (C) 2021  Aloys Liska
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * importWithContentHub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Lomiri.Content 1.3
import Qt.labs.platform 1.0

import FileMngt 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'importwithcontenthub.aloysliska'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property string myImportedImgFolder: ""
    property int currentImg: 1
    
    Settings {  // to save in app configuration, the image file path
        id: settings
        property alias myImportedImgFolder: root.myImportedImgFolder
        property alias currentImg: root.currentImg
    }
    
    property string myImageUrl: myImportedImgFolder + "/UserImportedImg" + currentImg + ".jpg"
     
    PageStack {
        id: pageStack
        Component.onCompleted: {
            push(mainPage)
        }
    
        Page {
            id: mainPage
            anchors.fill: parent

            header: PageHeader {
                id: header
                title: i18n.tr('Import with Content Hub')
            }

            Grid {
                id: topgrid
                anchors.top: header.bottom
                anchors.left: parent.left
                anchors.leftMargin: units.gu(2)
                width: parent.width
                columns: 2
                topPadding: units.gu(2)
                spacing: units.gu(2)
            
                Button {
                    id: iconLoadImg
                    width: units.gu(6)
                    height: units.gu(6)
                    color: theme.palette.normal.background
                    iconName: "insert-image"
                    iconPosition: "center"
                    onClicked: {
                        console.log("Click button")
                        pageStack.push(Qt.resolvedUrl("ImgImportPage.qml"))
                    }
                }

                Label {
                    width: parent.width - iconLoadImg.width - units.gu(4)
                    wrapMode: Text.Wrap
                    text: {
                        if (myImportedImgFolder == "")
                            return (i18n.tr("Click button to select an image"))
                        else
                            return (i18n.tr("<b>Image file path:</b><br>") + "<i>" + myImageUrl + "</i>")
                    }
                }
            }
                
            Image {
                id: myimage
                anchors.top: topgrid.bottom
                width: parent.width
                height: parent.height - header.height - topgrid.height - units.gu(2)
                fillMode: Image.PreserveAspectCrop
                source: {
                    if (myImportedImgFolder == "")
                        return ("")
                    else
                        return ("file://" + myImageUrl)
                }
            }
        }
    }
}
