 /*
 * Copyright (C) 2021  Aloys Liska
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * importWithContentHub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.7
import Qt.labs.platform 1.0
import Lomiri.Components 1.3
import Lomiri.Content 1.3

import FileMngt 1.0

Page {
    id: imgImportPage
    
    property var activeTransfer     // tranfer that will be set by ContentPeerPicker
    property int temp: 1    
    
    visible: false
    header: PageHeader {
        id: headerInfo
        title: i18n.tr("Image Selection")
    }
    
    // Documentation for ContentHub
    // https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/Lomiri%20Content%20API.html

    //-----------------------------
    // Lomiri.Content ContentStore
    //-----------------------------
    // see https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/ContentStore.html
    // Use to define permanent storage of imported item
    ContentStore {
        id: imgStore
        
        // Properties of ContentStore:
        scope: ContentScope.App     // Lomiri.Content ContentScope
                                    //-----------------------------
                                    // see https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/ContentScope.html#sdk-ubuntu-content-contentscope
                                    // This is just an enumaration:
                                    //  System: to store in system space (is it possible, this should required special rights no?)
                                    //  User: to store in user space
                                    //->App: to store in application data space
                                    
        // uri: string which will contains the file path of the stored item. It will be set when importing item.
    }
    
    //----------------------------------
    // Lomiri.Content ContentPeerPicker
    //----------------------------------
    // see https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/ContentPeerPicker.html
    // Allow user to select the peer (an application) which will be the source for imported item (here a picture)
    ContentPeerPicker {
        anchors.fill: parent
        anchors.topMargin: imgImportPage.header.height
        visible: parent.visible
       
        // customerPeerModelLoader : not used here 
        // headerText: not used here
        showTitle: false    // false to not display the header (with headerText) of ContentPeerPicker
       
        // Properties for ContentPeerPicker: 
        contentType: ContentType.Pictures   // Lomiri.Content ContentType
                                            //----------------------------
                                            // see https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/ContentType.html#sdk-ubuntu-content-contenttype
                                            // This allow ContentPeerPicker to select the application managing the type of item defined in ContentType
                                            // This is just an enumaration:
                                            //  Unknown
                                            //  Documents
                                            //->Pictures: this will allow import from applications managing pictures: gallery, camera,...
                                            //  Music
                                            //  Contacts
                                            //  Videos
                                            //  Links
                                            //  EBooks
                                            //  Text
                                            //  All: Any of the above content types
        
        handler: ContentHandler.Source      // Lomiri.Content ContentHandler
                                            //-------------------------------
                                            // see https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/ContentHandler.html
                                            // This define the transfer type: import, export or sharing
                                            // This is just an enumaration:
                                            //->Source: for import
                                            //  Destination: for export
                                            //  Share: for sharing
        
        // peer: the peer (application) selected by user, this peer will be the source for selection of item to import
        //       It will be defined on signal onPeerSelected (see below)
        
        
        // Signals for ContentPeerPicker
        onPeerSelected: {
            // Lomiri.Content ContentPeer
            //----------------------------
            // see https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/ContentPeer.html
            
            // Properties
            //  appID : defined automatically (from user selection)
            //  contentType: defined automatically (to Pictures) from contentType of ContentPeerPicker
            //  handler: defined automatically (to Source) from handler of ContentPeerPicker
            //  name: name of the application peer (available for reading)
            peer.selectionType = ContentTransfer.Single
            
            //methods
            // Request transfer from peer to the ContentStore imgStore
            //  if permanent storage is not needed, ContentStore is not needed. 
            //  So use only imgImportPage.activeTransfer = peer.request()  
            imgImportPage.activeTransfer = peer.request(imgStore)   
                    // Lomiri.Content ContentTransfer
                    //--------------------------------
                    // see https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/ContentTransfer.html
                    // Properties
                    //     contentType: set automatically here to Pictures
                    //     destination: set automatically here to this app
                    //     direction: set automatically here to import
                    //     downloadId: not used here
                    //     items: the list of imported items, here only one item: an image
                    //     selectionType: set automatically here to single 
                    //     source: set automatically to the app ID being the source of imported image
                    //   ->state: state of the transfer, see monitoring of states below in Connections
                    //     store: set automatically to ContentStore imgStore
                    // 
                    // Methods:
                    //   ->finalized(): will be used to finalize the transfer
                    //          this will especially when closing the app, delete the imported file in cache folder
                    //          (but the image requested to be store permanently with ContentStore imgStore will be kept)
                    //     start(): not used here, it was automatically started on peer.request(imgStore)
        }
        
        onCancelPressed: {
            pageStack.pop()     // return to main view
        }
    }
    
    ContentTransferHint {
        // Lomiri.Content ContentTransferHint
        //------------------------------------
        // Component showing that the transfer is currently running
        // see https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/ContentTransferHint.html
        id: transferHint
        anchors.fill: parent
        
        // Properties
        activeTransfer: imgImportPage.activeTransfer    // the tranfer for which transferhint has to be displayed
    }
    
    Connections {
        target: imgImportPage.activeTransfer    
        onStateChanged: {
            // Monitoring of all possible state for the transfer activeTransfer
            // and performing of actions when needed for this app.  
            switch (imgImportPage.activeTransfer.state) {
                case ContentTransfer.Created:
                    console.log("Transfer Created")         // never seen in log, but should be the first state on transfer creation
                    break;
                case ContentTransfer.Initiated:
                    console.log("Transfer Initiated")       // occurs when starting the transfer
                    break;
                case ContentTransfer.InProgress:
                    console.log("Transfer InProgress")      // to view this log go back to the app to see the displayed transferHint 
                    break;
                case ContentTransfer.Downloading:
                    console.log("Transfer Downloading")     // not used here
                    break;
                case ContentTransfer.Downloaded:
                    console.log("Transfer Downloaded")      // not used here        
                    break;
                    
                case ContentTransfer.Charged:               
                    console.log("Transfer Charged")         // the image is imported in cache folder and in place defined by contentStore
                    console.log("Content Store: imgStore folder (uri):", imgStore.uri)
                    console.log("Content Transfer: image transfer store folder:",imgImportPage.activeTransfer.store)   // = contentscope.uri
                    console.log("content Transfer: image transfer url:", imgImportPage.activeTransfer.items[0].url)
                    
                    // The imported image is access below via activeTransfer.items:
                    //
                    // Lomiri.Content ContentItem
                    //----------------------------
                    // see https://api-docs.ubports.com/sdk/apps/qml/Lomiri.Content/ContentItem.html
                    // Properties:
                    //   text: the content of the transfer (not used here, content will be accessed as a file with url)
                    // ->url: url of the imported file
                    // Methods:
                    //   bool move(dir, fileName): to move file to dir and to rename it to filename
                    //      note: this method does not work for renaming an existing file.
                    //   bool move(dir): to move file to dir
                    //   string toDataURI(): content in base64 (note used here)

                    // Actions on the imported image: 
                    // The previous imported image files is deleted
                    //  Since Content Hub, QML, javascript, do not provide file operations (renaming, deletion, etc), 
                    //  except a limited move operation by ContentItem, then file operations are done
                    //  through a C++ plugin named in this app "FileMngt". 
                    if (FileMngt.remove(myImageUrl)) {
                        console.log("FILE DELETED: ", myImageUrl)
                    }                    
                    else {
                        console.log("FAIL TO DELETE (file may not exist): ", myImageUrl)
                    }
                    
                    myImportedImgFolder = imgStore.uri      // get the folder path of the imported image (in Content Store)
                    
                    // Use a trick to cause refresh of image display in main view
                    // the trick is to have two file name: filename1 and filename2.
                    // by switching between these two filename, the source for image (QML component) will change and cause the reload of image
                    if (currentImg == 1) {  // filename1 was used
                        temp = 2    // switch to filename2
                    }
                    else {  // filename2 was used
                        temp = 1    // switch to filename1
                    }
                    
                    // rename the file name of imported image in filename1 or filename2
                    // FIXME: do not limit to .jpg extension
                    if (FileMngt.rename(imgImportPage.activeTransfer.items[0].url, myImportedImgFolder + "/UserImportedImg" + temp + ".jpg")) {
                        currentImg = temp   // set currentImg to the new name (filename1 or filename2)
                                            // thanks to binding, this will cause update of myImageUrl which is the file path for source image QML component in main view.
                        console.log("FILE RENAMED to:", myImageUrl)
                    }
                    else {
                        console.log("FAIL TO RENAME FILE")
                    }
                    break;
                    
                case ContentTransfer.Collected:
                    console.log("Transfer Collected")
                    imgImportPage.activeTransfer.finalize()     // finalize the transfer
                    pageStack.pop()                             // return to main view
                    break;
                    
                case ContentTransfer.Aborted:
                    console.log("Transfer Aborted")             // occurs if user cancel
                    break;
                case ContentTransfer.Finalized:
                    console.log("Transfer Finalized")
                    break;
                default:
                    console.log("Transfer Unkonwn State")
            }            
        }
    }
}
