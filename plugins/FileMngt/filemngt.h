/*
 * Copyright (C) 2021  Aloys Liska
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * importWithContentHub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILEMNGT_H
#define FILEMNGT_H

#include <QObject>
#include <QFile>

class FileMngt: public QObject {
    Q_OBJECT

public:
    FileMngt();
    ~FileMngt() = default;

    Q_INVOKABLE bool remove(QString source);
    Q_INVOKABLE bool rename(QString source, const QString& fileName);
};

#endif
