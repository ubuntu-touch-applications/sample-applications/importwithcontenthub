/*
 * Copyright (C) 2021  Aloys Liska
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * importWithContentHub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include "filemngt.h"

FileMngt::FileMngt() {

}

bool FileMngt::remove(QString source)
{
    if (source.isEmpty())
        return false;

    if(source.startsWith("file://"))
        source.remove("file://");

    QFile file(source);
    return file.remove();
}

bool FileMngt::rename(QString source, const QString &fileName)
{
    if (source.isEmpty())
        return false;

    if(source.startsWith("file://"))
        source.remove("file://");

    QFile file(source);
    return file.rename(fileName);
}
