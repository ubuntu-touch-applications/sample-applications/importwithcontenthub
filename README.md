# Import with Content Hub

Sample application to import an image using Content Hub

## Documentation for ContentHub

- Guidelines: <https://docs.ubports.com/en/latest/appdev/guides/contenthub.html>
- API:  <https://ubports.gitlab.io/docs/api-docs/index.html?p=lomiri-content-qml-api%2Flomiri-content-qmlmodule.html>
- Examples in source code of Content Hub: <https://gitlab.com/ubports/development/core/content-hub/-/tree/main/examples?ref_type=heads>
- Posts in forum: 
    - <https://forums.ubports.com/topic/3659/using-image-from-content-hub>
    - <https://forums.ubports.com/topic/5792/store-a-file-imported-with-content-hub/14>
    
## Summary
This sample app allow user to import an image, the image is stored (so that it is available on next app launch).
And when user imports a new image, it is stored by overwriting the last one (only last imported image is stored).

Use and description of Content Hub in this app:

- Use `ContentPeerPicker` to provide user selection of the application being the source of the item (image) to import.
It display an interface for the user to select an application providing image, then to select the image to import.
- The `ContentPeerPicker` will reqest a `ContentTransfer` which includes the storage destination into a `ContentStore`
- Use `ContentTransferHint` to provide a display blocking the user to interact with the app during the transfer.
- The state of the transfer `ContentTransfer` is monitored by this app to detect when the image import is completed:
    - State starts with values `Created`, `Initiated`, `InProgress`, then
    - when state = `Charged`, the imported image can be retrieved from an url in a `ContentItem` given in output of `ContentTransfer`
      The file can then be used for the need of the app.
    - When state goes then to `Collected`, the app request finalization of the `ContentTransfer` with its method `finalize`. 
      So State ends with value `Finalized`.
    - if user cancel, the state will go to `aborted`.


## Build and launch
Build and Launch the app on your phone:

- `clickable`

Then get the logs from the app when it runs on your phone:

- `clickable logs`


## License

Copyright (C) 2021  Aloys Liska

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
